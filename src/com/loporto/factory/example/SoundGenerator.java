package com.loporto.factory.example;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/*leonardo oporto*/

public class SoundGenerator {

	public static void main(String[] args) throws LineUnavailableException, InterruptedException {
		float frequencyHz = Float.parseFloat(args[0]);   //933;
		int durationMs = Integer.parseInt(args[1]);// 5000;
	    
		int pauseDurationMs = Integer.parseInt(args[2]);//250; 
		int stepDurationMs = Integer.parseInt(args[3]);//250; //
	    
		
	    
	    for(int i=0; i<durationMs;i=i+stepDurationMs+pauseDurationMs) {
	    	
	    	reproduce(stepDurationMs,frequencyHz);
	    	//System.out.println("endRepro " + i);
		    Thread.sleep(pauseDurationMs);
	    }
	    

	}

	private static void reproduce(int durationMs, float frequencyHz) throws LineUnavailableException {
		byte[] buf = new byte[ 1 ];;
	    AudioFormat af = new AudioFormat( (float )44100, 8, 1, true, false );
	    SourceDataLine sdl = AudioSystem.getSourceDataLine( af );
	    sdl.open();
	    sdl.start();
		for( int i = 0; i < durationMs  * (float )44100 / 1000; i++ ) {
	    	double angle = i / ( (float )44100 / frequencyHz ) * 2.0 * Math.PI;
	        buf[ 0 ] = (byte )( Math.sin( angle ) * 100 );
	        sdl.write( buf, 0, 1 );
		}
		sdl.drain();
	    sdl.stop();
	}
}
