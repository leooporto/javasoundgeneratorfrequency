build with

'''
mvn compile build package
'''

Go to target and run
'''
java -jar audioGeneratorFrecuency-0.0.1-SNAPSHOT.jar FREQ_HZ TOTAL_DURATION_MS PAUSE_MS STEP_MS
'''

FREQ_HZ : frequency in HZ. 440 for example.
TOTAL_DURATION_MS: Total duration of execution. Example 10000 ms, will play in total 10 seconds.
PAUSE_MS: Duration of pauses in MS. Example 200 ms, will pause the sound 200ms.
STEP_MS: Duration of playing between each pause. Example: 500 ms. Will sound 500 ms, then pause, then 500ms again, untill reach TOTAL_DURATION_MS.


Example:

java -jar audioGeneratorFrecuency-0.0.1-SNAPSHOT.jar 440 10000 200 500

Regards 
Leonardo Oporto.
